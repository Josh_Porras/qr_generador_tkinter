#!/usr/bin/env python3
from PIL import Image, ImageDraw, ImageFont
from PIL import *
image = Image.new('RGB', (1150,1150), (255, 255, 255)) #1250 y 900 representan el ancho y alto (255,255,255)representan el color blanco en formato RGB
draw = ImageDraw.Draw(image)
font = ImageFont.truetype('arial.ttf', size=45)
from tkinter import *
from tkinter import messagebox
import os
import qrcode
import datetime
import random
#import winsound
#espero que lo disfruten python en español :)



class index():

    def __init__(self,root):
        self.root = root
        self.root.title('QR_Generador')
        self.root.geometry('1250x600+0+0')
        self.root.configure(bg='black')
        self.root.configure(cursor='X_cursor')
        self.Ltitulo = Label(self.root,font=('arial',60,'bold'),text='ID Tarjetas',bg='black',fg='white')
        self.Ltitulo.place(x=350,y=0)

        #============tiempo=====================================#
        d_date = datetime.datetime.now()
        reg_format_date = d_date.strftime("%I:%M:%S %p")
        #==============Label_Tiempo=============================#
        self.Label = Label(self.root,text=reg_format_date,bg='Black',fg='white')
        self.Label.place(x=550,y=99)
        #========================Frames==========================#
        self.index_frame = Frame(self.root,height=400,width=500,bd=20,relief='groove',bg='gray')
        self.index_frame.place(x=0,y=150)
        
        self.title_frame = Frame(self.root,height=50,width=500,bd=10,relief='flat',bg='blue')
        self.title_frame.place(x=650,y=250)

        self.b_frame = Frame(self.root,height=400,width=780,bd=10,relief="groove")
        self.b_frame.place(x=0,y=495)


        #=======Varaibles=========#
        self.empresa = StringVar()
        self.nombre = StringVar()
        self.apellido = StringVar()
        self.genero = StringVar()
        self.edad = StringVar()
        self.G_sangre = StringVar()
        self.tel = StringVar()
        self.dire = StringVar()

        self.l_001 = Label(self.index_frame,font=('arial',20,'bold'),relief='flat',bg='gray',text='Empresa ')
        self.l_001.grid(row=0,column=0)
        self.e_001 = Entry(self.index_frame,font=('arial',20,'bold'),textvariable=self.empresa)
        self.e_001.grid(row=0,column=1)

        self.l_002 = Label(self.index_frame,font=('arial',20,'bold'),relief='flat',bg='gray',text='Nombre ')
        self.l_002.grid(row=1,column=0)
        self.e_002 = Entry(self.index_frame,font=('arial',20,'bold'),textvariable=self.nombre)
        self.e_002.grid(row=1,column=1)

        self.l_003 = Label(self.index_frame,font=('arial',20,'bold'),relief='flat',bg='gray',text='Apellido ')
        self.l_003.grid(row=2,column=0)
        self.e_003 = Entry(self.index_frame,font=('arial',20,'bold'),textvariable=self.apellido)
        self.e_003.grid(row=2,column=1)

        self.l_004 = Label(self.index_frame,font=('arial',20,'bold'),relief='flat',bg='gray',text='Genero ')
        self.l_004.grid(row=3,column=0)
        self.e_004 = Entry(self.index_frame,font=('arial',20,'bold'),textvariable=self.genero)
        self.e_004.grid(row=3,column=1)

        self.l_005 = Label(self.index_frame,font=('arial',20,'bold'),relief='flat',bg='gray',text='Edad ')
        self.l_005.grid(row=4,column=0)
        self.e_005 = Entry(self.index_frame,font=('arial',20,'bold'),textvariable=self.edad)
        self.e_005.grid(row=4,column=1)

        self.l_006 = Label(self.index_frame,font=('arial',20,'bold'),relief='flat',bg='gray',text='Grupo Sanguineao ')
        self.l_006.grid(row=5,column=0)
        self.e_006 = Entry(self.index_frame,font=('arial',20,'bold'),textvariable=self.G_sangre)
        self.e_006.grid(row=5,column=1)

        self.l_007 = Label(self.index_frame,font=('arial',20,'bold'),relief='flat',bg='gray',text='Telefono ')
        self.l_007.grid(row=6,column=0)
        self.e_007 = Entry(self.index_frame,font=('arial',20,'bold'),textvariable=self.tel)
        self.e_007.grid(row=6,column=1)

        self.l_008 = Label(self.index_frame,font=('arial',20,'bold'),relief='flat',bg='gray',text='Direccion ')
        self.l_008.grid(row=7,column=0)
        self.e_008 = Entry(self.index_frame,font=('arial',20,'bold'),textvariable=self.dire)
        self.e_008.grid(row=7,column=1)

#================================================Botones======================================================================================
        self.bt_00c = Button(self.b_frame ,font=('arial',20,'bold'),text='Crear',width=10,bd=8,relief='flat',command=self._variabes_)
        self.bt_00c.pack(side=LEFT,padx=5)

        self.bt_00s = Button(self.b_frame ,font=('arial',20,'bold'),text='Salir',width=9,bd=8,relief='flat',command=self.salir)
        self.bt_00s.pack(side=LEFT,padx=5)

        self.cls_00 = Button(self.b_frame,font=('arial',20,'bold'),text='Limpiar',width=10,bd=8,relief='flat',command=self.limpiar)
        self.cls_00.pack(side=LEFT,padx=5)
        
    def _variabes_(self):

        #=========================================EMPRESA
        (x, y) = (50, 50)
        message = self.empresa.get()
        self.empresa.set(message)
        color = 'rgb(245, 4, 26)'
        font = ImageFont.truetype('arial.ttf', size=100)
        draw.text((x, y), message, fill=color, font=font)

        #=========================================NOMBRE
        (x,y) = (50,150)
        message = self.nombre.get()
        self.nombre.set(message)
        color = 'rgb(162, 31, 29)'
        font = ImageFont.truetype('arial.ttf', size=80)
        draw.text((x, y),message, fill=color, font=font)

        #=========================================APELLIDO
        (x,y) = (450,150)
        message = self.apellido.get()
        self.apellido.set(message)
        color = 'rgb(162, 31, 29)'
        font = ImageFont.truetype('arial.ttf', size=80)
        draw.text((x, y), message, fill=color, font=font)

        #=========================================GENERO
        (x,y) = (50,350)
        message = self.genero.get()
        self.genero.set(message)
        color = 'rgb(0, 0, 0)'
        font = ImageFont.truetype('arial.ttf', size=80)
        draw.text((x, y), message, fill=color, font=font)

        #=========================================EDAD
        (x,y) = (50,455)
        message = self.edad.get()
        self.edad.set(message)
        color = 'rgb(0, 0, 0)'
        font = ImageFont.truetype('arial.ttf', size=80)
        draw.text((x, y), message, fill=color, font=font)

        #=========================================GRUPO_SANGUINEO
        (x,y) = (50,550)
        message = self.G_sangre.get()
        self.G_sangre.set(message)
        color = 'rgb(245, 4, 26)'
        font = ImageFont.truetype('arial.ttf', size=80)
        draw.text((x, y), message, fill=color, font=font)


        #=========================================TELEFONO
        (x,y) = (50,650)
        message = self.tel.get()
        self.tel.set(message)
        color = 'rgb(0, 0, 0)'
        font = ImageFont.truetype('arial.ttf', size=80)
        draw.text((x, y), message, fill=color, font=font)

        #=========================================DIRECCION
        (x,y) = (50,750)
        message = self.dire.get()
        self.dire.set(message)
        color = 'rgb(0, 0, 0)'
        font = ImageFont.truetype('arial.ttf', size=80)
        draw.text((x, y), message, fill=color, font=font)

        
        #==========================================NUMERO_ID
        (x, y) = (700, 310)
        self.id_no=random.randint(10000000,90000000)#creacion del id con random
        message = str('ID '+str(self.id_no))
        color = 'rgb(0, 0, 0)' # black color
        font = ImageFont.truetype('arial.ttf', size=60)
        draw.text((x, y), message, fill=color, font=font)

        #==========================================GENERADOR_QR
        image.save(str(self.nombre.get())+'.png')
        img = qrcode.make(str(self.empresa.get())+str(self.id_no))#codigo QR 
        img.save(str(self.id_no)+'.bmp')

        #================================================IMAGEN_CODIGO_QR=============================================================================
        self.imagen = PhotoImage(file=(str(self.id_no)+'.bmp'))
        self.Label_imagen = Label(self.title_frame,image=self.imagen,width=290,height=290)
        self.Label_imagen.pack()

        #generador()
        #==========================Guardar_imagen("tarjeta")_con_sus_parametros
        def generador():
            self.imagen = Image.open(open(self.empresa.get()+'.png'))
            self.im = Image.open(str(self.id_no)+'.bmp')
            self.imagen.paste(self.im,(600,350))
            self.imagen.save(self.nombre.get()+'.png')



        #si todo esta correcto este mensaje se aparesera en pantalla de lo contrario no se genera nada
        messagebox.showinfo(title=self.nombre.get(),message='Datos creados')
   
            
        
    def limpiar(self):
        messagebox.showinfo(title="limpio",message=' OK PARA CONTINUAR')
        self.empresa.set(" ")
        self.nombre.set(" ")
        self.apellido.set(" ")
        self.genero.set(" ")
        self.edad.set(" ")
        self.G_sangre.set(" ")
        self.tel.set(" ")
        self.dire.set(" ")
        

    def salir(self):
        self.root.destroy()
       

#
def main():
    root = Tk()
    app = index(root)

if __name__ == '__main__':
    main()
